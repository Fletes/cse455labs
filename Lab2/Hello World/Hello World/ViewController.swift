//
//  ViewController.swift
//  Hello World
//
//  Created by jwfletes on 9/2/15.
//  Copyright © 2015 BHP. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var label: UILabel!
    
    @IBOutlet var textField: UITextField!

    @IBAction func submit(_ sender: AnyObject) {
        
        print("Button Tapped")
        
        label.text = textField.text
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Hello Jarred")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

