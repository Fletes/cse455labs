

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var label: UILabel!
    
    @IBOutlet var textField: UITextField!

    @IBAction func submit(_ sender: AnyObject) {
        getString(var1: textField.text!) { (result) in
            DispatchQueue.main.async(execute:{
                self.label.text = result
            })
        }
    }
    
    
    func getString(var1: String, completion: @escaping (_ result: String)-> Void) -> Void{
        let myURL = URL(string: "http://date.jsontest.com/")
        var request = URLRequest(url: myURL!)
        request.httpMethod = "GET"
        let postString = "thing1=\(var1)&"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in
            if error != nil {
                print("error \(error)")
                return
            }
            if let response = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                completion(response as String)
            }
            else
            {
                completion("some error occured")
            }
        })
        task.resume()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}












   /*
            //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid
            
            let parameters = ["time": textField.text!, "Date": textField.text!] as Dictionary<String, String>
            
            //create the url with URL
            let url = URL(string: "http://date.jsontest.com/")! //change the url
            
            //create the session object
            let session = URLSession.shared
            
            //now create the URLRequest object using the url object
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        // handle json...
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
        }
    
        private func httpRequest() {
            
            //create the url with NSURL
            let url = URL(string: "http://date.jsontest.com/")! //change the url
            
            //create the session object
            let session = URLSession.shared
            
            //now create the URLRequest object using the url object
            let request = URLRequest(url: url)
            
            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        print(json)
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
    }

    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //print("Button Tapped")
        
        //label.text = textField.text
        
        /*
        var wasSuccessful = false
        
        let attemptedUrl = URL(string: "http://date.jsontest.com/")
        
        if let url = attemptedUrl {
            
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
                
                if let urlContent = data {
                    
                    let webContent = NSString(data: urlContent, encoding: String.Encoding.utf8.rawValue)
                    
                    let websiteArray = webContent!.components(separatedBy: "\n")
                    
                    if websiteArray.count > 1 {
                        
                        let weatherArray = websiteArray[1].components(separatedBy: "\"")
                        
                        if weatherArray.count > 1 {
                            
                            wasSuccessful = true
                            
                            let weatherSummary = weatherArray[0].replacingOccurrences(of: "&deg;", with: "º")
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                
                                self.label.text = weatherSummary
                                
                            })
                            
                            
                        }
                    }
                    
                    
                }
                
                if wasSuccessful == false {
                    
                    self.label.text = "Couldn't Find The Weather For That City. Please Try Again"
                }
            })
            
            task.resume()
        }
        else {
            
            self.label.text = "Couldn't Find The Weather For That City. Please Try Again"
            
        }
        
     */
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print("Hello Jarred")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
*/
